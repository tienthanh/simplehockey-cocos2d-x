var GOAL_WIDTH = 400;
var GameLayer = cc.Layer.extend({
    ctor: function () {
        this._super();
        this._player1 = null; // GameSprite for player1
        this._player2 = null; // GameSprite for player2
        this._players = [];
        this._ball = null;    // GameSprite for ball

        this._player1ScoreLabel = null; //Label for player1's score
        this._player2ScoreLabel = null; //Label for player2's score

        //init data
        this._player1Score = 0; //data of player1's score
        this._player2Score = 0; //data of player2's score

        //get screen size
        this._screenSize =  cc.director.getWinSize();
        this.createGameUI();
        this.createListener();

    },
    createGameUI: function() {

        //1. add court image
        var court = new cc.Sprite(res.court_png);
        court.setPosition(cc.p(this._screenSize.width * 0.5, this._screenSize.height * 0.5));
        this.addChild(court);

        //2. add players
        this._player1 =  new GameSprite(res.mallet_png);
        this._player1.setPosition(cc.p(this._screenSize.width * 0.5, this._player1.radius() * 2));
        this.addChild(this._player1);
        this._players.push(this._player1);

        this._player2 =  new GameSprite(res.mallet_png);
        this._player2.setPosition(cc.p(this._screenSize.width * 0.5, this._screenSize.height - this._player1.radius() * 2));
        this.addChild(this._player2);
        this._players.push(this._player2);

        //3. add puck
        this._ball = new GameSprite(res.puck_png);
        this._ball.setPosition(cc.p(this._screenSize.width * 0.5, this._screenSize.height * 0.5));
        this.addChild(this._ball);


        //4. add score display
        this._player1ScoreLabel = new cc.LabelTTF("0", "Arial", 60);
        this._player1ScoreLabel.setPosition(cc.p(this._screenSize.width - 60, this._screenSize.height * 0.5 - 80));
        this._player1ScoreLabel.setRotation(90);
        this.addChild(this._player1ScoreLabel);

        this._player2ScoreLabel = new cc.LabelTTF("0", "Arial", 60);
        this._player2ScoreLabel.setPosition(cc.p(this._screenSize.width - 60, this._screenSize.height * 0.5 + 80));
        this._player2ScoreLabel.setRotation(90);
        this.addChild(this._player2ScoreLabel);

    },
    createListener: function() {

        //listen for touches
        cc.eventManager.addListener({
            swallowTouch: true,
            event: cc.EventListener.TOUCH_ALL_AT_ONCE,
            onTouchesBegan: this.onTouchesBegan.bind(this),
            onTouchesMoved: this.onTouchesMoved.bind(this),
            onTouchesEnded: this.onTouchesEnded.bind(this)
        }, this);


        //create main loop
        this.scheduleUpdate();
    },
    update: function(dt) {
        //update puck
        var ballNextPosition = this._ball.getNextPosition();
        var ballVector = cc.pMult(this._ball.getVector(), 0.98);

        ballNextPosition.x += ballVector.x;
        ballNextPosition.y += ballVector.y;

        //test for puck and mallet collision
        var squared_radii = Math.pow(this._player1.radius() + this._ball.radius(), 2);

        for (var player of this._players) {

            var playerNextPosition = player.getNextPosition();
            var playerVector = player.getVector();

            var diffx = ballNextPosition.x - player.getPositionX();
            var diffy = ballNextPosition.y - player.getPositionY();

            var distance1 = Math.pow(diffx, 2) + Math.pow(diffy, 2);
            var distance2 = Math.pow(this._ball.getPositionX() - playerNextPosition.x, 2) + Math.pow(this._ball.getPositionY() - playerNextPosition.y, 2);

            if (distance1 <= squared_radii || distance2 <= squared_radii) {

                var mag_ball = Math.pow(ballVector.x, 2) + Math.pow(ballVector.y, 2);
                var mag_player = Math.pow(playerVector.x, 2) + Math.pow(playerVector.y, 2);

                var force = Math.sqrt(mag_ball + mag_player);
                var angle = Math.atan2(diffy, diffx);

                ballVector.x = force* Math.cos(angle);
                ballVector.y = (force* Math.sin(angle));

                ballNextPosition.x = playerNextPosition.x + (player.radius() + this._ball.radius() + force) * Math.cos(angle);
                ballNextPosition.y = playerNextPosition.y + (player.radius() + this._ball.radius() + force) * Math.sin(angle);

                cc.audioEngine.playEffect(res.hit_sound);
            }

            //check collision of ball and sides
            if (ballNextPosition.x < this._ball.radius()) {
                ballNextPosition.x = this._ball.radius();
                ballVector.x *= -0.8;
                cc.audioEngine.playEffect(res.hit_sound);
            }

            if (ballNextPosition.x > this._screenSize.width - this._ball.radius()) {
                ballNextPosition.x = this._screenSize.width - this._ball.radius();
                ballVector.x *= -0.8;
                cc.audioEngine.playEffect(res.hit_sound);
            }
            //ball and top of the court
            if (ballNextPosition.y > this._screenSize.height - this._ball.radius()) {
                if (this._ball.getPosition().x < this._screenSize.width* 0.5 - GOAL_WIDTH* 0.5 || this._ball.getPosition().x > this._screenSize.width* 0.5 + GOAL_WIDTH* 0.5) {
                    ballNextPosition.y = this._screenSize.height - this._ball.radius();
                    ballVector.y *= -0.8;
                    cc.audioEngine.playEffect(res.hit_sound);
                }
            }
            //ball and bottom of the court
            if (ballNextPosition.y < this._ball.radius() ) {
                if (this._ball.getPosition().x < this._screenSize.width* 0.5 - GOAL_WIDTH* 0.5 ||
                this._ball.getPosition().x > this._screenSize.width* 0.5 + GOAL_WIDTH* 0.5) {
                    ballNextPosition.y = this._ball.radius();
                    ballVector.y *= -0.8;
                    cc.audioEngine.playEffect(res.hit_sound);
                }
            }

            //finally, after all checks, update ball's vector and next position
            this._ball.setVector(ballVector);
            this._ball.setNextPosition(ballNextPosition);


            //check for goals!
            if (ballNextPosition.y  < -this._ball.radius() * 2) {
                this.playerScore(2);

            }
            if (ballNextPosition.y > this._screenSize.height + this._ball.radius() * 2) {
                this.playerScore(1);
            }

            //move pieces to next position
            this._player1.setPosition(this._player1.getNextPosition());
            this._player2.setPosition(this._player2.getNextPosition());
            this._ball.setPosition(this._ball.getNextPosition());
        }
    },
    playerScore: function(player) {
        cc.audioEngine.playEffect(res.score_sound);

        this._ball.setVector(cc.p(0,0));
        //if player 1 scored...
        if (player === 1) {

            this._player1Score++;
            this._player1ScoreLabel.setString(this._player1Score);
            //move ball to player 2 court
            this._ball.setNextPosition(cc.p(this._screenSize.width * 0.5, this._screenSize.height * 0.5 + 2 * this._ball.radius()));

            //if player 2 scored...
        } else {

            this._player2Score++;
            this._player2ScoreLabel.setString(this._player2Score);
            //move ball to player 1 court
            this._ball.setNextPosition(cc.p(this._screenSize.width * 0.5, this._screenSize.height * 0.5 - 2 * this._ball.radius()));

        }
        //move players to original position
        this._player1.setPosition(cc.p(this._screenSize.width * 0.5, this._player1.radius() * 2));
        this._player2.setPosition(cc.p(this._screenSize.width * 0.5, this._screenSize.height - this._player1.radius() * 2));

        //clear current touches
        this._player1.setTouch(null);
        this._player2.setTouch(null);
    },
    onTouchesBegan: function(touches, event) {
        for (var i = 0; i < touches.length; i++) {
            if (touches[i] != null) {
                var tap = touches[i].getLocation();


                for (var player of this._players) {
                    var rect = player.getBoundingBox();
                    if (cc.rectContainsPoint(rect, tap)) {
                        player.setTouch(touches[i]);
                    }

                }
            }
        }

    },
    onTouchesMoved: function(touches, event) {
        //loop through all moving touches
        for (var touch of touches) {
            if (touch != null) {
                var tap = touch.getLocation();

                for (var player of this._players) {
                    if (player.getTouch() != null && cc.pointEqualToPoint(player.getTouch().getLocation(), touch.getLocation())) {
                        var nextPosition = tap;

                        //keep player inside screen
                        if (nextPosition.x < player.radius())
                            nextPosition.x = player.radius();
                        if (nextPosition.x > this._screenSize.width - player.radius())
                            nextPosition.x = this._screenSize.width - player.radius();
                        if (nextPosition.y < player.radius())
                            nextPosition.y  = player.radius();
                        if (nextPosition.y > this._screenSize.height - player.radius())
                            nextPosition.y = this._screenSize.height - player.radius();

                        //keep player inside its court
                        if (player.getPositionY() < this._screenSize.height* 0.5) {
                            if (nextPosition.y > this._screenSize.height* 0.5 - player.radius()) {
                                nextPosition.y = this._screenSize.height* 0.5 - player.radius();
                            }
                        } else {
                            if (nextPosition.y < this._screenSize.height* 0.5 + player.radius()) {
                                nextPosition.y = this._screenSize.height* 0.5 + player.radius();
                            }
                        }

                        player.setNextPosition(nextPosition);
                        player.setVector(cc.p(tap.x - player.getPositionX(), tap.y - player.getPositionY()));
                    }

                }
            }
        }


    },
    onTouchesEnded: function(touches, event) {
        //loop through all ending touches
        for( var touch of touches) {
            if(touch != null) {
                for (var player of this._players) {
                    if (player.getTouch() != null && cc.pointEqualToPoint(player.getTouch().getLocation(), touch.getLocation())) {
                        //if touch ending belongs to this player, clear it
                        player.setTouch(null);
                        player.setVector(cc.p(0,0));
                    }
                }
            }
        }
    }
});


var GameScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new GameLayer();
        this.addChild(layer);
    }
});

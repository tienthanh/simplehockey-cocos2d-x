var GameSprite = cc.Sprite.extend({
    ctor: function (res) {
        this._super(res);
        this._nextPosition = null;
        this._vector = cc.p(0, 0);
        this._touch = null;
    },
    getTouch: function() {
        return this._touch;
    },
    setTouch: function(touch) {
        this._touch = touch;
    },
    getNextPosition: function() {
        return this._nextPosition;
    },
    setNextPosition: function(point) {
        this._nextPosition = point;
    },
    setVector: function(vec) {
        this._vector = vec;
    },
    getVector: function() {
        return this._vector;
    },
    setPosition: function(newPosOrxValue, yValue) {
        this._super(newPosOrxValue);
        if (!cc.pointEqualToPoint(this.getNextPosition(), newPosOrxValue)) {
            this.setNextPosition(point);
        }
    },
    radius: function() {
        return this.getContentSize().width*0.5;
    }
});
